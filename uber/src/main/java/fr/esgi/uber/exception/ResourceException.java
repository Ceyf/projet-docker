package fr.esgi.uber.exception;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class ResourceException extends ManagerException {
	
	public ResourceException(HttpStatus status, String functionalMsg) {
		super(status, functionalMsg);
	}
}
