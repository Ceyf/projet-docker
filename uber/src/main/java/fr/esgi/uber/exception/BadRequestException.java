package fr.esgi.uber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends ResourceException {
	private static final long serialVersionUID = 1L;
	
	public BadRequestException(HttpStatus status, String functionalMsg) {
			super(status, functionalMsg);
		}
}
