package fr.esgi.uber.exception;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class ManagerException extends RuntimeException {
	private HttpStatus status;
	private String functionalMsg;
	private Object[] args;
	
	public ManagerException(final HttpStatus status, final String functionalMsg, Object... args) {
		super(functionalMsg);
		this.status = status;
		this.functionalMsg = functionalMsg;
		this.args = args;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getFunctionalMsg() {
		return functionalMsg;
	}

	public void setFunctionalMsg(String functionalMsg) {
		this.functionalMsg = functionalMsg;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}	
}
