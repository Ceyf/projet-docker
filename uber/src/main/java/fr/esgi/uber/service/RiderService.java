package fr.esgi.uber.service;

import fr.esgi.uber.model.Rider;

import java.util.List;

public interface RiderService {
	Rider addRider(Rider rider);
	List<Rider> getRiders();
	Rider getRiderById(Long id);
	Rider updateRider(Rider newRider, Long id);
	String deleteRider(Long id);
}
