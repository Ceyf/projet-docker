package fr.esgi.uber.service.impl;

import fr.esgi.uber.exception.BadRequestException;
import fr.esgi.uber.exception.NotFoundException;
import fr.esgi.uber.exception.ResourceException;
import fr.esgi.uber.model.Driver;
import fr.esgi.uber.repository.DriverRepository;
import fr.esgi.uber.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriverServiceImpl implements DriverService {
    @Autowired
    private DriverRepository driverRepository;

    @Override
    public Driver addDriver(Driver driver) throws ResourceException {
        if(driver.getName() == null || driver.getEmail() == null || driver.getDateOfBirth() == null
                || driver.getFirstName() == null || driver.getCar() == null
                || driver.getLicense() == null|| driver.getName().isEmpty()
                || driver.getEmail().isEmpty() || driver.getDateOfBirth().isEmpty()
                || driver.getFirstName().isEmpty() || driver.getCar().isEmpty()
                || driver.getLicense().isEmpty()
        ) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Can not create the driver");
        }

        return driverRepository.save(driver);
    }

    @Override
    public List<Driver> getAllDrivers() {
        return driverRepository.findAll();
    }

    @Override
    public Driver getDriver(Long id) {
        Driver driver = driverRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND, "Driver with id : " + id + " is NOT FOUND !"));
        return driver;
    }

    @Override
    public String deleteDriver(Long id) {
        driverRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND, "Cant't delete Driver with id : " + id + " !"));
        driverRepository.deleteById(id);
        return "Driver with id: " + id + " is deleted !";
    }

    public Driver updateDriver(Driver newDriver, Long id) {
        Driver res = driverRepository.findById(id)
                .map(driver -> {
                    driver.setName(newDriver.getName());
                    driver.setFirstName(newDriver.getFirstName());
                    driver.setAge(newDriver.getAge());
                    driver.setDateOfBirth(newDriver.getDateOfBirth());
                    driver.setEmail(newDriver.getEmail());
                    driver.setCar(newDriver.getCar());
                    driver.setLicense(newDriver.getLicense());
                    driver.setRating(newDriver.getRating());
                    driver.setRuns(newDriver.getRuns());

                    return driverRepository.save(driver);
                }).orElseThrow(() ->
                        new NotFoundException(HttpStatus.NOT_FOUND, "Driver with id : " + id + " is NOT FOUND !"));
        return res;
    }
}
