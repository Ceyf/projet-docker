package fr.esgi.uber.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import fr.esgi.uber.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import fr.esgi.uber.exception.BadRequestException;
import fr.esgi.uber.exception.ResourceException;
import fr.esgi.uber.model.Rider;
import fr.esgi.uber.repository.RiderRepository;
import fr.esgi.uber.service.RiderService;
import org.springframework.web.server.ResponseStatusException;

@Service
public class RiderServiceImpl implements RiderService {
	@Autowired
    private RiderRepository riderRepository;

	@Override
	public Rider addRider(Rider rider) throws ResourceException {
		if(rider.getName() == null || rider.getEmail() == null || rider.getCardNumber() == null
				|| rider.getDateOfBirth() == null || rider.getFirstName() == null 
				||rider.getName().isEmpty() || rider.getEmail().isEmpty() || rider.getCardNumber().isEmpty()
				|| rider.getDateOfBirth().isEmpty() || rider.getFirstName().isEmpty() || rider.equals(null)
		) {
			throw new BadRequestException(HttpStatus.BAD_REQUEST, "Can not create the rider");
		}
		
		Date today = Calendar.getInstance().getTime();
		rider.setCreateDate(today);	
		
		return riderRepository.save(rider);
	}

	@Override
	public List<Rider> getRiders() {
		return riderRepository.findAll();
	}

	@Override
	public Rider getRiderById(Long id) {
		Rider rider = riderRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND, "Rider with id : " + id + " is NOT FOUND !"));
		return rider;
	}

	@Override
	public Rider updateRider(Rider newRider, Long id) {
		Rider res = riderRepository.findById(id)
				.map(rider -> {
					rider.setName(newRider.getName());
					rider.setFirstName(newRider.getFirstName());
					rider.setAge(newRider.getAge());
					rider.setDateOfBirth(newRider.getDateOfBirth());
					rider.setCardNumber(newRider.getCardNumber());
					rider.setEmail(newRider.getEmail());

					return riderRepository.save(rider);
				}).orElseThrow(() ->
						new NotFoundException(HttpStatus.NOT_FOUND, "Rider with id : " + id + " is NOT FOUND !"));
		return res;
	}

	@Override
	public String deleteRider(Long id) {
		riderRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND, "Cant't delete Rider with id : " + id + " !"));
		riderRepository.deleteById(id);
		return "Rider with id: " + id + " is deleted !";
	}
}

