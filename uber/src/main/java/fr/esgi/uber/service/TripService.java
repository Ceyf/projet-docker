package fr.esgi.uber.service;

import fr.esgi.uber.model.RequesterRole;
import fr.esgi.uber.model.Trip;
import fr.esgi.uber.model.TripDto;

import java.util.List;

public interface TripService {


    public List<Trip> getMysTrips(RequesterRole requesterRole);

    public TripDto createTrip(Trip trip);

    public String deleteTrip(Long id);

}
