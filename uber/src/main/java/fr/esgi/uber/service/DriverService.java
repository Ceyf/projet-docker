package fr.esgi.uber.service;

import fr.esgi.uber.model.Driver;

import java.util.List;

public interface DriverService {
    Driver addDriver(Driver driver);
    List<Driver> getAllDrivers();
    Driver getDriver(Long id);
    String deleteDriver(Long id);
    Driver updateDriver(Driver newDriver, Long id);
}
