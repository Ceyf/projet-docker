package fr.esgi.uber.service.impl;

import fr.esgi.uber.exception.BadRequestException;
import fr.esgi.uber.exception.NotFoundException;
import fr.esgi.uber.exception.ResourceException;
import fr.esgi.uber.mapper.TripMapper;
import fr.esgi.uber.model.RequesterRole;
import fr.esgi.uber.model.Trip;
import fr.esgi.uber.model.TripDto;
import fr.esgi.uber.repository.TripRepository;
import fr.esgi.uber.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TripServiceImpl implements TripService {

    @Autowired
    TripRepository tripRepository;


    private TripMapper tripMapper;

    @Override
    public List<Trip> getMysTrips(RequesterRole requesterRole) {
        if (requesterRole.role == (null) || requesterRole.id == (null)) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Role does not exist");
        }
        String upperStringRole = requesterRole.role.toUpperCase();
        if (upperStringRole.equals("RIDER")) {
            return tripRepository.getAllByRider_id((Long) requesterRole.id);
        } else if (upperStringRole.equals("DRIVER")) {
            return tripRepository.getAllByDriver_id((Long) requesterRole.id);
        }
        return new ArrayList<Trip>();
    }

    @Override
    public TripDto createTrip(Trip trip) throws ResourceException {
        if (trip.getLieu_debut() == null || trip.getArrivePlace() == null || trip.getStartHour() == null
                || trip.getArriveHour() == null || trip.getRider() == null
        ) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Can not create the trip");
        }
        tripMapper = new TripMapper();
        Trip t = tripRepository.save(trip);
        return tripMapper.mapModelToDto(t);
    }

    @Override
    public String deleteTrip(Long id) throws ResourceException {
        Trip trip = tripRepository.getOne(id);
        if (trip == null) {
            throw new NotFoundException(HttpStatus.NOT_FOUND, "ce trajet n'existe pas");
        }
        tripRepository.deleteById(id);
        return "Le trajet a bien été supprimer";
    }
}
