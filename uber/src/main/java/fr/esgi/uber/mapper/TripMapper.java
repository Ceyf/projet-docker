package fr.esgi.uber.mapper;

import fr.esgi.uber.model.Trip;
import fr.esgi.uber.model.TripDto;
import org.springframework.stereotype.Component;

@Component
public class TripMapper {

    public TripDto mapModelToDto(Trip trip) {
        TripDto result = new TripDto();
        result.id = trip.getid();
        result.setArriveHour(trip.getArriveHour());
        result.setArrivePlace(trip.getArrivePlace());
        result.setLieu_debut(trip.getLieu_debut());
        result.setStartHour(trip.getStartHour());
        return result;
    }
}
