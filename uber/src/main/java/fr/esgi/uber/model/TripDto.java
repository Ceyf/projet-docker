package fr.esgi.uber.model;

public class TripDto {

    public Long id;
    public String startHour;
    public String arriveHour;
    public String arrivePlace;
    public String startPlace;

    public Long getid() {
        return id;
    }

    public void setTrip_id(Long trip_id) {
        this.id = trip_id;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getArriveHour() {
        return arriveHour;
    }

    public void setArriveHour(String arriveHour) {
        this.arriveHour = arriveHour;
    }

    public String getArrivePlace() {
        return arrivePlace;
    }

    public void setArrivePlace(String arrivePlace) {
        this.arrivePlace = arrivePlace;
    }

    public String getLieu_debut() {
        return startPlace;
    }

    public void setLieu_debut(String lieu_debut) {
        this.startPlace = lieu_debut;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }
}
