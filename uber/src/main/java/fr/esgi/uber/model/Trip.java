package fr.esgi.uber.model;


import javax.persistence.*;

@Entity
@Table(name = "trip", schema = "public")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "rider")
    public Rider rider;

    @ManyToOne
    @JoinColumn(name = "driver")
    public Driver driver;

    @Column(name = "start_hour")
    public String startHour;
    @Column(name = "arrive_hour")
    public String arriveHour;
    @Column(name = "arrive_place")
    public String arrivePlace;
    @Column(name = "start_place")
    public String startPlace;

    public Long getid() {
        return id;
    }

    public void setTrip_id(Long trip_id) {
        this.id = trip_id;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public Rider getRider() {
        return rider;
    }

    public void setRider(Rider rider) {
        this.rider = rider;
    }

    public String getArriveHour() {
        return arriveHour;
    }

    public void setArriveHour(String arriveHour) {
        this.arriveHour = arriveHour;
    }

    public String getArrivePlace() {
        return arrivePlace;
    }

    public void setArrivePlace(String arrivePlace) {
        this.arrivePlace = arrivePlace;
    }

    public String getLieu_debut() {
        return startPlace;
    }

    public void setLieu_debut(String lieu_debut) {
        this.startPlace = lieu_debut;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }
}
