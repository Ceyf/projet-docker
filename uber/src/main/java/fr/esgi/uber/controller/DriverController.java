package fr.esgi.uber.controller;

import fr.esgi.uber.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.esgi.uber.model.Driver;

import java.util.List;

@RestController
@RequestMapping("/driver")
public class DriverController {
    @Autowired
    private DriverService driverService;

    @PostMapping
    ResponseEntity<Driver> createDriver(@RequestBody Driver driver) {
        return new ResponseEntity<Driver>(driverService.addDriver(driver), HttpStatus.CREATED);
    }

    @GetMapping
    ResponseEntity<List<Driver>> getAllDrivers() {
        return new ResponseEntity<List<Driver>>(driverService.getAllDrivers(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    ResponseEntity<Driver> getDriver(@PathVariable long id) {
        return new ResponseEntity<Driver>(driverService.getDriver(id), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    ResponseEntity<String> deleteDriver(@PathVariable long id) {
        return new ResponseEntity<String>(driverService.deleteDriver(id), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    ResponseEntity<Driver> updateDriver(@RequestBody Driver driver, @PathVariable long id) {
        return new ResponseEntity<Driver>(driverService.updateDriver(driver, id), HttpStatus.OK);
    }
}
