package fr.esgi.uber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.esgi.uber.model.Rider;
import fr.esgi.uber.service.RiderService;

import java.util.List;

@RestController
@RequestMapping("/rider")
public class RiderController {
	@Autowired
	private RiderService riderService;
	
    @PostMapping
    ResponseEntity<Rider> createRider(@RequestBody Rider rider) {
        return new ResponseEntity<Rider>(riderService.addRider(rider), HttpStatus.CREATED);
    }

    @GetMapping
    ResponseEntity<List<Rider>> getRiders() {
        return new ResponseEntity<List<Rider>>(riderService.getRiders(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    ResponseEntity<Rider> getRiderById(@PathVariable long id) {
        return new ResponseEntity<Rider>(riderService.getRiderById(id), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    ResponseEntity<Rider> updateRider(@RequestBody Rider rider, @PathVariable long id) {
        return new ResponseEntity<Rider>(riderService.updateRider(rider, id), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    ResponseEntity<String> deleteRider(@PathVariable long id) {
        return new ResponseEntity<String>(riderService.deleteRider(id), HttpStatus.OK);
    }
}
