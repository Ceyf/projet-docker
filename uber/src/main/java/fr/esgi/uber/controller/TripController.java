package fr.esgi.uber.controller;

import fr.esgi.uber.model.RequesterRole;
import fr.esgi.uber.model.Trip;
import fr.esgi.uber.model.TripDto;
import fr.esgi.uber.service.TripService;
import fr.esgi.uber.service.impl.TripServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/trips")
public class TripController {
    @Autowired
    TripService tripServiceImpl;


    @PostMapping(path = "/my_trips")
    public ResponseEntity<List<Trip>> getMyTrips(@RequestBody RequesterRole requesterRole) {
        List<Trip> tripList = tripServiceImpl.getMysTrips(requesterRole);
        return new ResponseEntity<List<Trip>>(tripList, HttpStatus.OK);
    }

    @PostMapping(path = "")
    ResponseEntity<TripDto> createTrip(@RequestBody Trip trip) {
        TripDto tripResult = tripServiceImpl.createTrip(trip);
        return new ResponseEntity<TripDto>(tripResult, HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    ResponseEntity<String> deleteTrip(@PathVariable Long id) {
        String tripResult = tripServiceImpl.deleteTrip(id);
        return new ResponseEntity<String>(tripResult, HttpStatus.OK);
    }
}
