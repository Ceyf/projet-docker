package fr.esgi.uber.repository;

import fr.esgi.uber.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {

    public List<Trip> getAllByRider_id(Long id);

    public List<Trip> getAllByDriver_id(Long id);

//    public

}


