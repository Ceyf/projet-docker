package fr.esgi.uber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.esgi.uber.model.Driver;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

}