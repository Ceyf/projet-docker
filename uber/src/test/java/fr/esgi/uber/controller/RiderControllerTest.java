package fr.esgi.uber.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.esgi.uber.model.Rider;
import fr.esgi.uber.service.RiderService;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class RiderControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	final ObjectMapper mapper = new ObjectMapper();

	@MockBean
    private RiderService riderService;
	
	private Rider rider;
	
	@BeforeEach
	public void setup() {
		rider = new Rider();	
		rider.setId(1L);
		rider.setName("Dupont");
		rider.setFirstName("Paul");
		rider.setAge(22);
		rider.setDateOfBirth("22/02/1998");
		rider.setEmail("pauldupont@gmail.com");
		rider.setCardNumber("1235-6936-5475");
	}
	
	@Test
	public void createRiderTest() throws Exception {	
		mockMvc.perform(post("/rider")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rider)))
		      	.andExpect(status().isCreated());
	}
	
	@Test
	public void createRiderWithNoBodyTest() throws Exception {	
		mockMvc.perform(post("/rider")
				.contentType(MediaType.APPLICATION_JSON))
		      	.andExpect(status().isBadRequest());
	}

	@Test
	public void getRidersTest() throws Exception {
		List<Rider> riders = Arrays.asList(rider);

		when(riderService.getRiders()).thenReturn(riders);

		mockMvc.perform(get("/rider")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void getRiderByIdTest() throws Exception {
		when(riderService.getRiderById(1L)).thenReturn(rider);
		mockMvc.perform(get("/rider/{id}", rider.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(rider.getId()))
				.andExpect(jsonPath("$.name").value(rider.getName()))
				.andExpect(jsonPath("$.firstName").value(rider.getFirstName()))
				.andExpect(jsonPath("$.age").value(rider.getAge()))
				.andExpect(jsonPath("$.email").value(rider.getEmail()))
				.andExpect(jsonPath("$.dateOfBirth").value(rider.getDateOfBirth()))
				.andExpect(jsonPath("$.cardNumber").value(rider.getCardNumber()));
	}

	@Test
	public void updateRiderTest() throws Exception {
		Rider newRider = rider;
		when(riderService.updateRider(rider,1L)).thenReturn(newRider);

		newRider.setName("Da Corte");
		newRider.setFirstName("Julien");

		mockMvc.perform(put("/rider/{id}", 1L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(newRider)))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteRiderTest() throws Exception {
		when(riderService.deleteRider(2L)).thenReturn("Rider has been deleted !");
		mockMvc.perform(delete("/rider/{id}", 2L))
				.andExpect(status().isOk());
	}
}
