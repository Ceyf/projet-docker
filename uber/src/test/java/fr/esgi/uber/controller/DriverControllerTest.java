package fr.esgi.uber.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.uber.model.Driver;
import fr.esgi.uber.service.DriverService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DriverControllerTest {
    @Autowired
    private MockMvc mockMvc;

    final ObjectMapper mapper = new ObjectMapper();
    @MockBean
    private DriverService DriverService;

    private Driver driver;

    @BeforeEach
    public void setup() {
        driver = new Driver();
        driver.setName("Dupont");
        driver.setFirstName("Paul");
        driver.setAge(22);
        driver.setDateOfBirth("22/02/1998");
        driver.setEmail("pauldupont@gmail.com");
        driver.setCar("La Voiture à Paulo");
        driver.setLicense("/licenses/pauldupont.jpg");
    }

    @Test
    public void createDriverTest() throws Exception {
        mockMvc.perform(post("/driver")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(driver)))
                .andExpect(status().isCreated());
    }

    @Test
    public void createDriverWithNoBodyTest() throws Exception {
        mockMvc.perform(post("/driver")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAllDriversTest() throws Exception {
        List<Driver> allDrivers = Arrays.asList(driver);

        when(DriverService.getAllDrivers()).thenReturn(allDrivers);

        mockMvc.perform(get("/rider")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getDriverTest() throws Exception {
        when(DriverService.getDriver(1L)).thenReturn(driver);
        mockMvc.perform(get("/driver/{id}", driver.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(driver)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteDriverTest() throws Exception {
        when(DriverService.deleteDriver(2L)).thenReturn("Driver has been deleted !");
        mockMvc.perform(delete("/driver/{id}", 2L))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDriverTest() throws Exception {
        Driver newDriver = driver;
        when(DriverService.updateDriver(driver,1L)).thenReturn(newDriver);

        newDriver.setName("Da Corte");
        newDriver.setFirstName("Julien");

        mockMvc.perform(put("/driver/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(newDriver)))
                .andExpect(status().isOk());
    }
}
