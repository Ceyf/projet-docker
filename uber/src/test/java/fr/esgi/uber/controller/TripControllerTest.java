package fr.esgi.uber.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.uber.model.RequesterRole;
import fr.esgi.uber.model.Rider;
import fr.esgi.uber.model.Trip;
import fr.esgi.uber.service.RiderService;
import fr.esgi.uber.service.TripService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TripControllerTest {
    @Autowired
    private MockMvc mockMvc;

    final ObjectMapper mapper = new ObjectMapper();
    @MockBean
    private TripService tripService;

    private List<Trip> tripList;
    private RequesterRole requesterRole;
    private  Trip trip;

    @BeforeEach
    public void setup() {
        tripList = new ArrayList<>();

        Rider rider = new Rider();
        rider.setId(1L);
        rider.setName("Dupont");
        rider.setFirstName("Paul");
        rider.setAge(22);
        rider.setDateOfBirth("22/02/1998");
        rider.setEmail("pauldupont@gmail.com");
        rider.setCardNumber("1235-6936-5475");

        trip = new Trip();
        trip.setTrip_id(1L);
        trip.setRider(rider);
        trip.setStartHour("12:50");
        trip.setArriveHour("16:15");
        trip.setArrivePlace("Place de la Nation");
        trip.setLieu_debut("bastille");
        tripList.add(trip);

        requesterRole = new RequesterRole();
        requesterRole.id = 1L;
        requesterRole.role = "rider";
    }

    @Test
    public void getTripsTest() throws Exception {
        mockMvc.perform(post("/trips/my_trips")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(tripList)));
    }

    @Test
    public void getTripsWithNoBodyTest() throws Exception {
        mockMvc.perform(post("/trips/my_trips")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    /*@Test
    public void createTripTest() throws Exception {
        mockMvc.perform(post("/trips")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(trip)))
                .andExpect(status().isCreated());
    }*/

    @Test
    public void createTripWithNoBodyTest() throws Exception {
        mockMvc.perform(post("/trips")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteTripTest() throws Exception {
        when(tripService.deleteTrip(1L)).thenReturn("Le trajet a bien été supprimer");
        mockMvc.perform(delete("/trips/{id}", 1L))
                .andExpect(status().isOk());
    }
}
