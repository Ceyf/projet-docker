package fr.esgi.uber.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.esgi.uber.model.Rider;
import fr.esgi.uber.repository.RiderRepository;
import fr.esgi.uber.service.impl.RiderServiceImpl;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RiderServiceTest {
	
	@InjectMocks
    private RiderService riderService = new RiderServiceImpl();

    @Mock
    private RiderRepository riderRepository;   
    
    @BeforeEach
	public void init() {
    	MockitoAnnotations.initMocks(this);
	}
    
    @Test
	public void addRiderTest() {		
    	Rider rider = new Rider();	
		rider.setId(1L);
		rider.setName("Dupont");
		rider.setFirstName("Paul");
		rider.setAge(22);
		rider.setDateOfBirth("22/02/1998");
		rider.setEmail("pauldupont@gmail.com");
		rider.setCardNumber("1235-6936-5475");
		
		when(riderService.addRider(rider)).thenReturn(rider);
		
		Rider riderTest = riderService.addRider(rider);
		
		assertEquals(riderTest.getId(), 1L);	
		assertEquals("Dupont", riderTest.getName());
		assertEquals("Paul", riderTest.getFirstName());
		assertEquals(22, riderTest.getAge());
		assertEquals("22/02/1998", riderTest.getDateOfBirth());
		assertEquals("pauldupont@gmail.com", riderTest.getEmail());
		assertEquals("1235-6936-5475", riderTest.getCardNumber());	
	}

	@Test
	public void getRidersTest() {
		Rider rider = new Rider();
		rider.setId(1L);
		rider.setName("Dupont");
		rider.setFirstName("Paul");
		rider.setAge(22);
		rider.setDateOfBirth("22/02/1998");
		rider.setEmail("pauldupont@gmail.com");
		rider.setCardNumber("1235-6936-5475");

		List<Rider> riders = Arrays.asList(rider);

		when(riderService.getRiders()).thenReturn(riders);

		List<Rider> ridersTest = riderService.getRiders();

		assertThat(riders, is(ridersTest));
		assertThat(ridersTest, hasSize(1));
		assertThat(ridersTest.size(), is(1));
		assertThat(ridersTest, hasItems(rider));
	}

	@Test
	public void getRiderByIdTest() {
		Rider rider = new Rider();
		rider.setId(1L);
		rider.setName("Dupont");
		rider.setFirstName("Paul");
		rider.setAge(22);
		rider.setDateOfBirth("22/02/1998");
		rider.setEmail("pauldupont@gmail.com");
		rider.setCardNumber("1235-6936-5475");
		when(riderRepository.findById(1L)).thenReturn(Optional.of(rider));

		Rider riderTest = riderService.getRiderById(1L);

		assertEquals(riderTest.getId(), 1L);
		assertEquals("Dupont", riderTest.getName());
		assertEquals("Paul", riderTest.getFirstName());
		assertEquals(22, riderTest.getAge());
		assertEquals("22/02/1998", riderTest.getDateOfBirth());
		assertEquals("pauldupont@gmail.com", riderTest.getEmail());
		assertEquals("1235-6936-5475", riderTest.getCardNumber());
	}

	@Test
	public void updateRiderTest() {
		Rider rider = new Rider();
		rider.setId(1L);
		rider.setName("Dupont");
		rider.setFirstName("Paul");
		rider.setAge(22);
		rider.setDateOfBirth("22/02/1998");
		rider.setEmail("pauldupont@gmail.com");
		rider.setCardNumber("1235-6936-5475");
		when(riderRepository.findById(1L)).thenReturn(Optional.of(rider));

		Rider riderTest = riderService.getRiderById(1L);

		riderTest.setName("Da corte");
		riderTest.setFirstName("Julien");
		riderTest.setAge(25);

		assertEquals(riderTest.getId(), 1L);
		assertEquals("Da corte", riderTest.getName());
		assertEquals("Julien", riderTest.getFirstName());
		assertEquals(25, riderTest.getAge());
		assertEquals("22/02/1998", riderTest.getDateOfBirth());
		assertEquals("pauldupont@gmail.com", riderTest.getEmail());
		assertEquals("1235-6936-5475", riderTest.getCardNumber());
	}
}
