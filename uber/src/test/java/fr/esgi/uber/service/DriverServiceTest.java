package fr.esgi.uber.service;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.esgi.uber.model.Driver;
import fr.esgi.uber.repository.DriverRepository;
import fr.esgi.uber.service.impl.DriverServiceImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DriverServiceTest {

    @InjectMocks
    private DriverService driverService = new DriverServiceImpl();

    @Mock
    private DriverRepository driverRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addDriverTest() {
        Driver driver = new Driver();
        driver.setId(1L);
        driver.setName("Dupont");
        driver.setFirstName("Paul");
        driver.setAge(22);
        driver.setDateOfBirth("22/02/1998");
        driver.setEmail("pauldupont@gmail.com");
        driver.setCar("La Voiture à Paulo");
        driver.setLicense("licenses/pauldupont.jpg");

        when(driverService.addDriver(driver)).thenReturn(driver);

        Driver driverTest = driverService.addDriver(driver);

        assertEquals(driverTest.getId(), 1L);
        assertEquals("Dupont", driverTest.getName());
        assertEquals("Paul", driverTest.getFirstName());
        assertEquals(22, driverTest.getAge());
        assertEquals("22/02/1998", driverTest.getDateOfBirth());
        assertEquals("pauldupont@gmail.com", driverTest.getEmail());
        assertEquals("La Voiture à Paulo", driverTest.getCar());
        assertEquals("licenses/pauldupont.jpg", driverTest.getLicense());
    }

    @Test
    public void getAllDriversTest() {
        Driver driver = new Driver();
        driver.setId(1L);
        driver.setName("Dupont");
        driver.setFirstName("Paul");
        driver.setAge(22);
        driver.setDateOfBirth("22/02/1998");
        driver.setEmail("pauldupont@gmail.com");
        driver.setCar("La Voiture à Paulo");
        driver.setLicense("licenses/pauldupont.jpg");

        List<Driver> drivers = Arrays.asList(driver);

        when(driverService.getAllDrivers()).thenReturn(drivers);

        List<Driver> driversTest = driverService.getAllDrivers();

        assertThat(drivers, is(driversTest));
        assertThat(driversTest, hasSize(1));
        assertThat(driversTest, hasItems(driver));
    }

    @Test
    public void getDriverTest() {
        Driver driver = new Driver();
        driver.setId(1L);
        driver.setName("Dupont");
        driver.setFirstName("Paul");
        driver.setAge(22);
        driver.setDateOfBirth("22/02/1998");
        driver.setEmail("pauldupont@gmail.com");
        driver.setCar("La Voiture à Paulo");
        driver.setLicense("licenses/pauldupont.jpg");

        when(driverRepository.findById(1L)).thenReturn(Optional.of(driver));

        Driver driverTest = driverService.getDriver(1L);

        assertEquals(driverTest.getId(), 1L);
        assertEquals("Dupont", driverTest.getName());
        assertEquals("Paul", driverTest.getFirstName());
        assertEquals(22, driverTest.getAge());
        assertEquals("22/02/1998", driverTest.getDateOfBirth());
        assertEquals("pauldupont@gmail.com", driverTest.getEmail());
        assertEquals("La Voiture à Paulo", driverTest.getCar());
        assertEquals("licenses/pauldupont.jpg", driverTest.getLicense());
    }

    @Test
    public void updateDriverTest() {
        Driver driver = new Driver();
        driver.setId(1L);
        driver.setName("Dupont");
        driver.setFirstName("Paul");
        driver.setAge(22);
        driver.setDateOfBirth("22/02/1998");
        driver.setEmail("pauldupont@gmail.com");
        driver.setCar("La Voiture à Paulo");
        driver.setLicense("licenses/pauldupont.jpg");

        when(driverRepository.findById(1L)).thenReturn(Optional.of(driver));

        Driver driverTest = driverService.getDriver(1L);

        driverTest.setName("Da corte");
        driverTest.setFirstName("Julien");
        driverTest.setAge(25);

        assertEquals(driverTest.getId(), 1L);
        assertEquals("Da corte", driverTest.getName());
        assertEquals("Julien", driverTest.getFirstName());
        assertEquals(25, driverTest.getAge());
        assertEquals("22/02/1998", driverTest.getDateOfBirth());
        assertEquals("pauldupont@gmail.com", driverTest.getEmail());
        assertEquals("La Voiture à Paulo", driverTest.getCar());
        assertEquals("licenses/pauldupont.jpg", driverTest.getLicense());
    }
}
