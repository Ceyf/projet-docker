package fr.esgi.uber.service;

import fr.esgi.uber.model.RequesterRole;
import fr.esgi.uber.model.Rider;
import fr.esgi.uber.model.Trip;
import fr.esgi.uber.repository.TripRepository;
import fr.esgi.uber.service.impl.TripServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TripServiceTest {

    @InjectMocks
    private TripService tripService = new TripServiceImpl();

    @Mock
    private TripRepository tripRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getTripTest() {
        List<Trip> tripList = new ArrayList<>();
        Rider rider = new Rider();
        rider.setId(1L);

        Trip trip = new Trip();
        trip.setTrip_id(1L);
        trip.setRider(rider);
        trip.setStartHour("12:50");
        trip.setArriveHour("16:15");
        trip.setArrivePlace("Place de la Nation");
        trip.setLieu_debut("bastille");
        tripList.add(trip);
        RequesterRole requesterRole = new RequesterRole();
        requesterRole.id = 1L;
        requesterRole.role = "rider";

        when(tripService.getMysTrips(requesterRole)).thenReturn(tripList);

        List<Trip> tripListTets = tripService.getMysTrips(requesterRole);
        tripListTets.add(trip);

        assertEquals(tripList, tripListTets);
    }
/*
    @Test
    public void createTripTest() {
        Rider rider = new Rider();
        rider.setId(1L);
        Trip tripResponse = new Trip();
        tripResponse.setTrip_id(1L);
        tripResponse.setRider(rider);
        tripResponse.setStartHour("12:50");
        tripResponse.setArriveHour("16:15");
        tripResponse.setArrivePlace("Place de la Nation");
        tripResponse.setLieu_debut("bastille");

        TripDto tripDto = new TripMapper().mapModelToDto(tripResponse);

        when(tripService.createTrip(tripResponse)).thenReturn(tripDto);

        TripDto tripTest = tripService.createTrip(tripResponse);

        assertEquals(tripTest, tripDto);
    }

 */
}
