# Dockerize Everything

Projet de Février 2020 
Outils et techniques de développement


**Versionning:**

Nous avons choisi GitLab comme plateforme car nous souhaitons nous essayer à l'intégration continue (CI) même si aucun membre du groupe n'en a fait jusqu'ici. 
GitLab permet une suivi plus précis du projet que Git grâce à tous les outils qu'il offre.

Nous avons décidé de suivre le worflow Feature Branch car, une fois de plus, c'est un outil adapté à la CI puisque le principe de base est de créer une branche pour chaque feature
développée, ce qui permet de toujours garder une branche master clean et fonctionnelle et d'ajouter une à une les feature pour éviter un gros merge qui pourrait produire beaucoup
de bugs répartis sur plusieurs features.

Nous avons adopté une convention de nommage de type "nom-action description" pour nos commits (exemple: Julien-add adding the code for the feature allowing to add a driver).


**Choix de la base de données:**

Nous avons décidé d'utiliser PostgreSQL pour notre base de données pour sa concentration sur l'Objet.


**Choix du Langage:**

Le projet est développé en Java (pour la progrmmation orientée objets), plus précisément avec le framework SpringBoot, puisque le coeur de l'application est une API et SpringBoot
permet de réaliser des API.


**Fonctionnalités de l'application:**

UberApi contient 3 CRUDs: -Rider
                          -Driver
                          -Trips
                          

**Contribuer:**

Il faut créer un fichier "application.properties" dans un dossier "ressources" et lui passer:


server.servlet.contextPath=/api/v1

server.port=9090

spring.datasource.url=jdbc:postgresql://192.168.99.100:5432/[database]

spring.datasource.username=[username]

spring.datasource.password=[password]


Avec l'environnement IntelliJ:
Après avoir effectué un mvn clean install, il faut exécuter le fichier SringBoot "UberApiApplication".


Avec Docker:
Pour déployer l'application, il faut se placer à la racine du répertoire Git et exécuter la commandes suivante: "docker-compose up --build". Si les containers sont déjà créés,
il faut exécuter la commande "docker-compose start". Pour mettre les containers en veille, il faut exécuter la commande "docker-compose stop".


**Tests:**

Les tests sont effectués avec Junit.

Les tests sur Controller sont: "createDriverTest", "createRiderTest", "createDriverWithNoBodyTest", "createRiderWithNoBodyTest", "getAllDriversTest", "getRidersTest", "getRidersByIdTest",
"getDriverTest", "deleteRiderTest", "updateRiderTest", "deleteDriverTest", "updateDriverTest", "getTripTest", "getTripsWithNoBodyTest", "createTripWithNoBodyTest", 
"deleteTripTest".

Les tests sur Service sont: "addRiderTest", "getRiderTest", "getRiderByIdTest", "updateRiderTest", "addDriverTest", "getDriverTest", "getAllDriversTest", "updateDriverTest",
"getTripTest".